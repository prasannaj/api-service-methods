const fetch = require('node-fetch');
require('dotenv').config()

const EMIRATE_GROUPWISE_EMIRATE = `
query EMIRATE_GROUPWISE($emirate: String!, $for_date: date!) { 

    employee_health_new_group_by_contract_type_count(where: {date: {_eq: $for_date}, emirate: {_eq: $emirate}}) { 
      contract_type 
      count 
      date   
      department 
      emirate   
      employee_group 
      staff_id 
    } 

    employee_health_new_group_covid(where: {date: {_eq: $for_date}, emirate: {_eq: $emirate}}) { 
        count 
        date 
        emirate 
        employee_group 
      } 
  }  
 
`

const EMIRATE_GROUPWISE_ALL = `
query EMIRATE_GROUPWISE($for_date: date!) { 

    employee_health_new_group_by_contract_type_count(where: {date: {_eq: $for_date}}) { 
      contract_type 
      count 
      date   
      department 
      emirate   
      employee_group 
      staff_id 
    } 


    employee_health_new_group_covid(where: {date: {_eq: $for_date}}) { 
        count 
        date 
        emirate 
        employee_group 
      } 
  
  }  
`

const getHomeData = async (emirate, for_date) => {

    if(emirate) query = EMIRATE_GROUPWISE_EMIRATE
    else query = EMIRATE_GROUPWISE_ALL

    const graphqlReq = { "query": query, "variables": { "emirate": emirate, "for_date": for_date } }
    const ge_respone = await fetch(process.env.GE_URL, {
        method: 'POST',
        headers: { 'content-type': 'application/json', 'x-hasura-admin-secret': process.env.GE_ADMIN_KEY },
        body: JSON.stringify(graphqlReq)
    });
    const response = await ge_respone.json();
    return response;
}

getHomeData("Abu Dhabi", "2020-07-02").then((data) => {
    console.log(JSON.stringify(data));
})